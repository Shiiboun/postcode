import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_DATA } from "../graphql/getData";
import { Pokemon } from "../components/pokemon.comonent";
import Table from "react-bootstrap/Table";

export function PokemonsContainer({ asc }) {
  const { data: { pokemons = [] } = {} } = useQuery(GET_DATA, {
    variables: { first: 9 },
  });
  if (asc) {
    pokemons.sort(function (a, b) {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
  } else {
    // desc
    pokemons.sort(function (a, b) {
      if (a.name < b.name) {
        return 1;
      }
      if (a.name > b.name) {
        return -1;
      }
      return 0;
    });
  }

  return (
    <Table bordered hover responsive striped>
      <thead>
        <tr>
          <th>Name</th>
          <th>Max HP</th>
          <th>Max CP</th>
        </tr>
      </thead>
      <tbody>
        {pokemons.map((pokemon) => (
          <Pokemon key={pokemon.id} pokemon={pokemon} />
        ))}
      </tbody>
    </Table>
  );
}
