import React from "react";
import ApolloClient from "apollo-boost";
import { Nav, Navbar } from "react-bootstrap";
import { ApolloProvider } from "@apollo/react-hooks";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { PokemonsContainer } from "./container/pokemon.container";

function App() {
  const client = new ApolloClient({
    uri: "https://graphql-pokemon.now.sh/",
  });
  return (
    <ApolloProvider client={client}>
      <Router>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">
              ASC
            </Nav.Link>
            <Nav.Link as={Link} to="/desc">
              DESC
            </Nav.Link>
          </Nav>
        </Navbar>
        <Switch>
          <Route path="/" exact>
            <PokemonsContainer asc={true} />
          </Route>
          <Route path="/desc">
            <PokemonsContainer asc={false} />
          </Route>
        </Switch>
      </Router>
    </ApolloProvider>
  );
}

export default App;
