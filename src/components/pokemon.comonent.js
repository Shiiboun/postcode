import React from "react";

export function Pokemon({ pokemon }) {
  return (
    <tr>
      <td className="pokemonName">{pokemon.name}</td>
      <td className="pokemonName">{pokemon.maxHP}</td>
      <td className="pokemonName">{pokemon.maxCP}</td>
    </tr>
  );
}
