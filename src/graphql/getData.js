import gql from "graphql-tag";

export const GET_DATA =  gql `
    query pokemons($first: Int!){
        pokemons(first: $first) {
            id
            name
            maxHP
            maxCP
        }
    }
`